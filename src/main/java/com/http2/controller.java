package com.http2;

import okhttp3.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController

public class controller {

    @RequestMapping("/workget")
        public ResponseEntity<String> getWork() {
        System.out.println("here");
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://localhost:8443/http2/work")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                    System.out.println("reach");
            }

        });
        return new ResponseEntity<>("Work yes", HttpStatus.OK);
    }

    @RequestMapping("/work")
    public ResponseEntity<String> work() {
        return new ResponseEntity<>("Work assigned", HttpStatus.OK);
    }


}
